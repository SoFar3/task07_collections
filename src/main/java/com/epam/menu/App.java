package com.epam.menu;

import com.epam.menu.controller.Controller;

public class App {

    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.start();
    }

}
