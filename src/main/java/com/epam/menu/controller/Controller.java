package com.epam.menu.controller;

import com.epam.menu.view.*;

public class Controller {

    private EnumView enumView;

    public Controller() {
        enumView = new EnumView();

        EnumMenu enumMenu = new EnumMenu(1);

        enumMenu.addMenuOption(MenuEnum.OPTION_1, new Option("Option 1"));
        enumMenu.addMenuOption(MenuEnum.Q, new Option("Option 1", () -> System.exit(0)));

        enumView.setMenu(enumMenu);
    }

    public void start() {
        enumView.showMenu();
    }

}
