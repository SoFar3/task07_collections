package com.epam.menu.view;

public enum MenuEnum {

    OPTION_1("1"),
    OPTION_2("2"),
    OPTION_3("3"),
    OPTION_4("4"),
    Q("Q"),
    N("N");

    private String value;

    MenuEnum(String value) {
        this.value = value;
    }

    static MenuEnum getValue(String value) {
        for(MenuEnum e: MenuEnum.values()) {
            if(e.value.equals(value)) {
                return e;
            }
        }
        return null;
    }

}
