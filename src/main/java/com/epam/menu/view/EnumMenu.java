package com.epam.menu.view;

import java.util.EnumMap;
import java.util.Map;

public class EnumMenu extends Menu<MenuEnum> {

    protected Map<MenuEnum, Option> menu;

    public EnumMenu() {
        this.menu = new EnumMap<MenuEnum, Option>(MenuEnum.class);
    }

    public EnumMenu(int repeat) {
        this();
        this.repeat = repeat;
    }

    public EnumMenu(Menu parent) {
        this();
        this.parent = parent;
    }

    public EnumMenu(Menu parent, int repeat) {
        this(parent);
        this.repeat = repeat;
    }

    @Override
    public void addMenuOption(MenuEnum key, Option option) {
        menu.put(key, option);
    }

    public Map<MenuEnum, Option> getMenu() {
        return menu;
    }

    @Override
    public void setParent(Menu<MenuEnum> parent) {

    }

    @Override
    public Menu<MenuEnum> getParent() {
        return parent;
    }

}
