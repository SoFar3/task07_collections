package com.epam.menu.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class EnumView implements View<MenuEnum> {

    private EnumMenu menu;
    private BufferedReader reader;

    public EnumView() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public void setMenu(EnumMenu menu) {
        this.menu = menu;
    }

    public void showMenu() {
        int counter = 1;
        while (true) {
            for (Map.Entry<MenuEnum, Option> option: menu.getMenu().entrySet()) {
                System.out.println(String.format("%s - %s", option.getKey(), option.getValue().getName()));
            }
            waitForUserOption();
            if (counter >= menu.getRepeat()) {
                if (menu.getMenu().get(MenuEnum.N) != null) {
                    menu.getMenu().get(MenuEnum.N).getAction().run();
                } else {
                    menu.getMenu().get(MenuEnum.Q).getAction().run();
                }
                counter = 0;
            }
            counter++;
        }
    }

    public String userInput(String message) {
        String input = "";
        try {
            System.out.println(message);
            input = reader.readLine();
        } catch (IOException ignored) { }
        return input;
    }

    public void waitForUserOption() {
        System.out.println("Enter your choice:");
        while (true) {
            try {
                MenuEnum choice = MenuEnum.getValue(reader.readLine());
                if (doAction(choice)) {
                    break;
                }
                System.out.println("Incorrect input");
            } catch (IOException ignored) { }
        }
    }

    public boolean doAction(MenuEnum key) {
        Option option = menu.getMenu().get(key);
        if (option == null) {
            return false;
        } else {
            option.getAction().run();
            return true;
        }
    }

}
