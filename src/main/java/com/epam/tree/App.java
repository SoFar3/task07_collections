package com.epam.tree;

public class App {

    public static void main(String[] args) {
        BinaryTree<Integer> integerBinaryTree = new BinaryTree<>();
        integerBinaryTree.add(5);
        integerBinaryTree.add(1);
        integerBinaryTree.add(7);
        integerBinaryTree.add(2);
        integerBinaryTree.add(15);
        integerBinaryTree.add(3);
        integerBinaryTree.add(9);
        integerBinaryTree.add(8);
        integerBinaryTree.add(14);
        integerBinaryTree.add(7);

        integerBinaryTree.printTree();
        System.out.println();
        integerBinaryTree.remove(5);
        integerBinaryTree.printTree();
        System.out.println();
        integerBinaryTree.remove(9);
        integerBinaryTree.printTree();
        System.out.println();
        integerBinaryTree.remove(1);
        integerBinaryTree.printTree();
        System.out.println();
        integerBinaryTree.remove(14);
        integerBinaryTree.printTree();
    }

}
