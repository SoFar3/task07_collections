package com.epam.tree;

public class BinaryTree<T extends Comparable<T>> {

    private Node<T> root;

    public void add(T value) {
        root = add0(value, root);
    }

    public Node<T> add0(T value, Node<T> node) {
        if (node == null) {
            node = new Node<>(value, null, null, null);
        } else {
            if (value.compareTo(node.value) >= 0) {
                node.right = add0(value, node.right);
                node.right.parent = node;
            } else {
                node.left = add0(value, node.left);
                node.left.parent = node;
            }
        }
        return node;
    }

    public T get(T object) {
        return get0(object, root).value;
    }

    private Node<T> get0(T object, Node<T> node) {
        if (node != null) {
            if (object.compareTo(node.value) == 0) {
                return node;
            } else {
                if (object.compareTo(node.value) < 0) {
                    return get0(object, node.left);
                } else {
                    return get0(object, node.right);
                }
            }
        }
        return null;
    }

    public void remove(T object) {
        Node<T> tNode = get0(object, root);
        if (tNode.left != null && tNode.right != null) {
            Node<T> max = findMax(tNode.left, tNode.left);
            if (tNode.left == max) {
                max.right = tNode.right;
                tNode.right.parent = max;
                if (tNode.parent.right == tNode) {
                    tNode.parent.right = max;
                } else {
                    tNode.parent.left = max;
                }
            } else {
                if (max.parent.right == max) {
                    max.parent.right = null;
                } else {
                    max.parent.left = null;
                }
                max.parent = tNode.parent;
                max.left = tNode.left;
                max.left.parent = max;
                max.right = tNode.right;
                max.right.parent = max;
                if (tNode.parent != null) {
                    if (tNode.parent.right == tNode) {
                        tNode.parent.right = max;
                    } else {
                        tNode.parent.left = max;
                    }
                } else {
                    root = max;
                }
            }
        } else if (tNode.left != null) {
            tNode.left.parent = tNode.parent;
            if (tNode.parent.right == tNode) {
                tNode.parent.right = tNode.left;
            } else {
                tNode.parent.left = tNode.left;
            }
        } else if (tNode.right != null) {
            tNode.right.parent = tNode.parent;
            if (tNode.parent.right == tNode) {
                tNode.parent.right = tNode.right;
            } else {
                tNode.parent.left = tNode.right;
            }
        } else {
            if (tNode.parent.right == tNode) {
                tNode.parent.right = null;
            } else {
                tNode.parent.left = null;
            }
            tNode.parent = null;
        }
    }

    private Node<T> findMax(Node<T> node, Node<T> lastMax) {
        if (node != null) {
            if (node.value.compareTo(lastMax.value) > 0) {
                lastMax = node;
            }
            lastMax = findMax(node.right, lastMax);
        }
        return lastMax;
    }

    public void printTree() {
        printTree0(root);
    }

    private void printTree0(Node<T> node) {
        if (node != null) {
            System.out.print(node.value + " ");
            printTree0(node.left);
            printTree0(node.right);
        }
    }

    private static class Node<T> {

        T value;

        Node<T> left;
        Node<T> right;
        Node<T> parent;

        public Node(T value, Node<T> left, Node<T> right, Node<T> parent) {
            this.value = value;
            this.left = left;
            this.right = right;
            this.parent = parent;
        }

    }

}
